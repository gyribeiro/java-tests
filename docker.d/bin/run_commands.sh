#!/usr/bin/env sh

BASE_DIR=/mnt

while getopts ":mrb" OPT_NAME 2> /dev/null; do
    case "${OPT_NAME}" in
        'm')
            # run make
            echo "Running make ${OPTARG}"
            cd ${BASE_DIR}
            make ${OPTARG}
            ;;
        'r')
            echo "Running gyrprog..."
            ${BASE_DIR}/gyrprog
            ;;
        'b')
            # run shel
            /bin/bash
            ;;
        'h')
            echo "Usage: kop_commands <options>"
            echo" Options:"
            echo " -m<target>: run makefile"
            echo " -r: run gyrprog"
            echo " -b: run bash"
            echo " -h: usage"
            ;;
        '?')
            echo "Invalid option: -${OPTARG}."
            exit 1
            ;;
        #':')
        #    echo "Required argument not found for option -${OPTARG}."
        #    exit 1
        #    ;;
        *)
            echo "Unknown error while processing options."
            exit 1
            ;;
    esac
done
