#!/usr/bin/env sh

DOCKERFILE_PATH=.
DISTRO=centos
IMAGE_NAME=gyr/${DISTRO}_java-tests
TAG_NAME=v1

docker pull ${DISTRO}:latest
docker build -t ${IMAGE_NAME}:${TAG_NAME} ${DOCKERFILE_PATH}
