JAVA = java
JAVAC = javac

MAIN = GyrProg

SRCS = $(shell ls *.java)

OBJS = $(patsubst %,%,$(SRCS:.java=.class))

all: $(MAIN)

$(MAIN): $(OBJS)
	$(JAVA) $(MAIN)

%.class: %.java
	$(JAVAC) $^

.PHONY: clean

clean:
	$(RM) -vf $(MAIN) $(OBJS)
